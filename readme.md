###libgpod-0.8.3 for windows

---
基于libgpod-0.8.3构建，原代码请访问[http://gtkpod.org/libgpod/](http://gtkpod.org/libgpod/)
效果图：
![](http://code.h4ck.org.cn/libgpod-0.8.3-for-windows/raw/870457e72dea5453a5c8e4b44d602eaf437e0933/screenshot/screenshot.png)

编译前请安装gtk，glib安装方法如下：

> Glib是一个多种用途的工具库，它提供许多有用的数据类型，宏定义，类型变换，字符串工具，文件工具，主循环的抽象等等。它可以用于许多类-UNIX平台、Windows，OS/2和BeOS中。GLib在GNU库通用公共许可（GNU LGPL)下发布。
> GLib的主要策略是除了数据结构管理功能以外所有的功能都是线程安全的。如果你有两个线程关联系统的数据结构，他们必须使用锁来同步他们的操作。


官方网站上提供了不少的文件和资源包，为了方便建议直接下载all-in-one bundle，猛击[此处](http://ftp.gnome.org/pub/gnome/binaries/win32/gtk+/2.24/gtk+-bundle_2.24.10-20120208_win32.zip)下载，猛击[此处](http://www.gtk.org/download/win32.php)访问官方下载页面。

下载后解压到某个目录下，我这里是解压到了D:\glib2.28.8，下面的配置以这个路径为例，如果你的不是，那么请自行修改相关路径。打开项目的属性，切换到VC++ Directory标签页，修改如下两项配置：

![](http://code.h4ck.org.cn/libgpod-0.8.3-for-windows/raw/870457e72dea5453a5c8e4b44d602eaf437e0933/screenshot/include.png)

在包含目录中加入如下内容：

    D:\glib2.28.8\include\atk-1.0;D:\glib2.28.8\lib\gtk-2.0\include;D:\glib2.28.8\include\pango-1.0;D:\glib2.28.8\include\cairo;D:\glib2.28.8\lib\glib-2.0\include;D:\glib2.28.8\include\glib-2.0;D:\glib2.28.8\include\gtk-2.0;D:\glib2.28.8\include;

在包含库目录中加入如下内容：

    D:\glib2.28.8\lib;

切换到Linker页面，在修改附加依赖项：
![](http://code.h4ck.org.cn/libgpod-0.8.3-for-windows/raw/870457e72dea5453a5c8e4b44d602eaf437e0933/screenshot/input.png)


在附加依赖项中加入如下内容：

    glib-2.0.lib;gtk-win32-2.0.lib;gdk-win32-2.0.lib;gobject-2.0.lib;gdk_pixbuf-2.0.lib;gthread-2.0.lib;gmodule-2.0.lib;pango-1.0.lib;atk-1.0.lib;zdll.lib;intl.lib

环境测试代码：
    #include <iostream>
    #include <regex>
    #include <windows .h>
    #include <gtk /gtk.h>
     
     
    using namespace std;
    int main( int argc, char **argv );
    int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,int nCmdShow){
    return main (__argc, __argv);
    }
     
     
    int main( int argc, char **argv ) {
    GtkWidget *window;
    gtk_init( &argc, &argv );
    window = gtk_window_new( GTK_WINDOW_TOPLEVEL );
    g_signal_connect( G_OBJECT( window ), "destroy",
    G_CALLBACK( gtk_main_quit ), NULL );
    gtk_widget_show( window );
    gtk_main ();
    return 0;
    }

更多信息请编译test下的相关代码即可。

[http：//www.h4ck.org.cn](http://www.h4ck.org.cn)